from pathlib import Path
from typing import Iterable

from spacy_llm.registry import registry
import jinja2
from spacy.tokens import Doc

@registry.llm_tasks("be_kdg_dai5.summarise_task")
def make_quote_extraction() -> "SummariseTask":
    return SummariseTask()

class SummariseTask:
    def __init__(self, template: str = "summarise_text", field: str = "summary"):
        path = f"./a3/templates/{template}.jinja"
        if not Path(path).exists():
            raise ValueError(f"{template} is not a valid template.")
        self._template = Path(path).read_text()
        self._field = field
        if not Doc.has_extension(field):
            Doc.set_extension(field, default=None)

    def generate_prompts(self, docs: Iterable[Doc]) -> Iterable[str]:
        template = jinja2.Environment().from_string(self._template)
        return (template.render(text=doc.text) for doc in docs)

    def parse_responses(self, docs: Iterable[Doc], responses: Iterable[str]) -> Iterable[Doc]:
        for doc, response in zip(docs, responses):
            try:
                setattr(doc._, self._field, response.replace("summary:", "").strip())
            except ValueError:
                setattr(doc._, self._field, None)
            yield doc
